package com.example.iandzillanmalik.farming_app_test.Fragment;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.iandzillanmalik.farming_app_test.EditProfileActivity;
import com.example.iandzillanmalik.farming_app_test.Model.User;
import com.example.iandzillanmalik.farming_app_test.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import static android.content.Context.MODE_PRIVATE;

public class ProfileFragment extends Fragment {
    TextView nama, username, email, nohp, alamat;
    Button edit;
    ImageView image_profile;
    FirebaseUser firebaseUser;

    String profileid;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v= inflater.inflate(R.layout.fragment_profile, container, false);

        nama = v.findViewById(R.id.nama);
        username  = v.findViewById(R.id.username);
        email  = v.findViewById(R.id.email);
        nohp  = v.findViewById(R.id.nohp);
        alamat  = v.findViewById(R.id.alamat);
        edit  = v.findViewById(R.id.btn_edit);
        image_profile = v.findViewById(R.id.image_profile);
        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        SharedPreferences prefs = getContext().getSharedPreferences("PREFS", MODE_PRIVATE);
        profileid = prefs.getString("profileid", "none");

        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), EditProfileActivity.class));
            }
        });

        userInfo();

        if (profileid.equals(firebaseUser.getUid())){
            edit.setText("Edit Profile");
        }else{
            edit.setVisibility(View.GONE);
        }

        return v;
    }

    private void userInfo(){
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Users").child(profileid);
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (getContext() == null){
                    return;
                }
                User user = dataSnapshot.getValue(User.class);
                Glide.with(getContext()).load(user.getImageurl()).into(image_profile);
                nama.setText(user.getNama());
                username.setText(user.getUsername());
                email.setText(user.getEmail());
                nohp.setText(user.getNoHp());
                alamat.setText(user.getAlamat());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}
