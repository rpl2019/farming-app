package com.example.iandzillanmalik.farming_app_test.Model;

public class ForumPost {
    private String postforumid;
    private String postimage;
    private String description;
    private String publisher;

    public ForumPost(String postforumid, String postimage, String description, String publisher) {
        this.postforumid = postforumid;
        this.postimage = postimage;
        this.description = description;
        this.publisher = publisher;
    }

    public ForumPost() {
    }

    public String getpostforumid() {
        return postforumid;
    }

    public void setpostforumid(String postforumid) {
        this.postforumid = postforumid;
    }

    public String getPostimage() {
        return postimage;
    }

    public void setPostimage(String postimage) {
        this.postimage = postimage;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }
}
