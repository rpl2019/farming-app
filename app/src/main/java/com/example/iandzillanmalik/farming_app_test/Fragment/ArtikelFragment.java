package com.example.iandzillanmalik.farming_app_test.Fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;


import com.example.iandzillanmalik.farming_app_test.R;

public class ArtikelFragment extends Fragment {

    public ArtikelFragment(){
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View v  = inflater.inflate(R.layout.fragment_artikel, container, false);
        WebView webView= (WebView)v.findViewById(R.id.webView1);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebViewClient(new WebViewClient());
        webView.loadUrl("https://alamtani.com/");
        return v;
    }
}