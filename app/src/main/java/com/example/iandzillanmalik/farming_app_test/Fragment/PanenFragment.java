package com.example.iandzillanmalik.farming_app_test.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.example.iandzillanmalik.farming_app_test.Adapter.PostPanenAdapter;
import com.example.iandzillanmalik.farming_app_test.Model.PanenPost;
import com.example.iandzillanmalik.farming_app_test.PostPanenActivity;
import com.example.iandzillanmalik.farming_app_test.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;


public class PanenFragment extends Fragment {
    FloatingActionButton fab;

    private RecyclerView recyclerView;
    private PostPanenAdapter postAdapter;
    private List<PanenPost> postList;
    ProgressBar progress_circular;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v= inflater.inflate(R.layout.fragment_panen, container, false);

        fab = v.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(getActivity(), PostPanenActivity.class);
                startActivity(in);
            }
        });

        recyclerView = v.findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        mLayoutManager.setReverseLayout(true);
        mLayoutManager.setStackFromEnd(true);
        recyclerView.setLayoutManager(mLayoutManager);
        postList = new ArrayList<>();
        postAdapter = new PostPanenAdapter(getContext(), postList);
        recyclerView.setAdapter(postAdapter);
        progress_circular = v.findViewById(R.id.progress_circular);

        readPosts();

        return v;
    }

    private void readPosts(){
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Posts Panen");

        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                postList.clear();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()){
                    PanenPost post = snapshot.getValue(PanenPost.class);
                    post.getPublisher();
                    postList.add(post);
                }
                postAdapter = new PostPanenAdapter(getActivity(),postList);
                recyclerView.setAdapter(postAdapter);
                postAdapter.notifyDataSetChanged();
                progress_circular.setVisibility(View.GONE);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}
