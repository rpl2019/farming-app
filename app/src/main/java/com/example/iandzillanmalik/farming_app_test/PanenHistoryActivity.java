package com.example.iandzillanmalik.farming_app_test;

import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import com.example.iandzillanmalik.farming_app_test.Adapter.MyPanenPostAdapter;
import com.example.iandzillanmalik.farming_app_test.Adapter.PostPanenAdapter;
import com.example.iandzillanmalik.farming_app_test.Model.PanenPost;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class PanenHistoryActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    private MyPanenPostAdapter postAdapter;
    private List<PanenPost> postList;
    ProgressBar progress_circular;
    FirebaseUser firebaseUser;
    String profileid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_panen_history);

        SharedPreferences prefs = this.getSharedPreferences("PREFS", MODE_PRIVATE);
        profileid = prefs.getString("profileid", "none");

        recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        mLayoutManager.setReverseLayout(true);
        mLayoutManager.setStackFromEnd(true);
        recyclerView.setLayoutManager(mLayoutManager);
        postList = new ArrayList<>();
        postAdapter = new MyPanenPostAdapter(this, postList);
        recyclerView.setAdapter(postAdapter);
        progress_circular = findViewById(R.id.progress_circular);

        myPost();
    }

    private void myPost(){
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Posts Panen");
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                postList.clear();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()){
                    PanenPost post = snapshot.getValue(PanenPost.class);
                    if (post.getPublisher().equals(profileid)){
                        postList.add(post);
                    }
                }
                Collections.reverse(postList);
                postAdapter.notifyDataSetChanged();
                progress_circular.setVisibility(View.GONE);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
