package com.example.iandzillanmalik.farming_app_test;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;

public class LupaPasswordActivity extends AppCompatActivity {
    EditText email;
    Button submit;
    FirebaseAuth auth;
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lupa_password);

        email = findViewById(R.id.editText_email);
        submit = findViewById(R.id.button_submit);

        auth = FirebaseAuth.getInstance();

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String Email = email.getText().toString();

                if (TextUtils.isEmpty(Email)) {
                    Toast.makeText(getApplicationContext(), "Masukkan email anda!", Toast.LENGTH_SHORT).show();
                    return;
                }else{
                    auth.sendPasswordResetEmail(Email)
                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isSuccessful()) {
                                        pd = new ProgressDialog(LupaPasswordActivity.this);
                                        pd.setMessage("Please wait...");
                                        pd.show();
                                        Toast.makeText(LupaPasswordActivity.this, "Cek email anda untuk reset password!", Toast.LENGTH_SHORT).show();
                                        startActivity(new Intent(LupaPasswordActivity.this, LoginActivity.class));
                                    } else {
                                        pd.dismiss();
                                        Toast.makeText(LupaPasswordActivity.this, "Gagal mengirim lupa password ke email anda!", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });
                }
            }
        });
    }

    public void Login(View view) {
        startActivity(new Intent(this, LoginActivity.class));
    }
}
