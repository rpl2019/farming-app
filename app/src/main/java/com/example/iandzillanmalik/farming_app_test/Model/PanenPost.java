package com.example.iandzillanmalik.farming_app_test.Model;

public class PanenPost {
    private String postpanenid;
    private String postimage;
    private String description;
    private String publisher;

    public PanenPost(String postpanenid, String postimage, String description, String publisher) {
        this.postpanenid = postpanenid;
        this.postimage = postimage;
        this.description = description;
        this.publisher = publisher;
    }

    public PanenPost() {
    }

    public String getpostpanenid() {
        return postpanenid;
    }

    public void setpostpanenid(String postpanenid) {
        this.postpanenid = postpanenid;
    }

    public String getPostimage() {
        return postimage;
    }

    public void setPostimage(String postimage) {
        this.postimage = postimage;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }
}
