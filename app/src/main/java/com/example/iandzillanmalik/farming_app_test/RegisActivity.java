package com.example.iandzillanmalik.farming_app_test;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;

public class RegisActivity extends AppCompatActivity {
    EditText username, nama, email, password, confirm, noHp, alamat;
    Button registrasi;
    FirebaseAuth auth;
    DatabaseReference reference;
    ProgressDialog pd;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_regis);

        username = findViewById(R.id.editText_username);
        nama = findViewById(R.id.editText_nama);
        email = findViewById(R.id.editText_email);
        password = findViewById(R.id.editText_pass);
        confirm = findViewById(R.id.editText_confirm);
        noHp = findViewById(R.id.editText_phone);
        alamat = findViewById(R.id.editText_alamat);
        registrasi = findViewById(R.id.button_regis);

        auth = FirebaseAuth.getInstance();

        registrasi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String Username = username.getText().toString();
                String Name = nama.getText().toString();
                String Email = email.getText().toString();
                String Password = password.getText().toString();
                String Confirm = confirm.getText().toString();
                String NoHp = noHp.getText().toString();
                String Alamat = alamat.getText().toString();

                if (TextUtils.isEmpty(Username) || TextUtils.isEmpty(Name) || TextUtils.isEmpty(Email) || TextUtils.isEmpty(Password) || TextUtils.isEmpty(Confirm) || TextUtils.isEmpty(NoHp) || TextUtils.isEmpty(Alamat)){
                    Toast.makeText(RegisActivity.this, "Isi semua data!", Toast.LENGTH_SHORT).show();
                } else if(Password.length() < 6){
                    Toast.makeText(RegisActivity.this, "Password harus memiliki 6 karakter!", Toast.LENGTH_SHORT).show();
                } else if (!password.getText().toString().equalsIgnoreCase(confirm.getText().toString())){
                    Toast.makeText(RegisActivity.this, "Password dan konfirmasi password tidak cocok!", Toast.LENGTH_SHORT).show();
                } else {
                    pd = new ProgressDialog(RegisActivity.this);
                    pd.setMessage("Please wait...");
                    pd.show();
                    registrasi(Username, Name, Email, Password, NoHp, Alamat);
                }
            }
        });
    }

    private void registrasi(final String username, final String nama, final String email, String password, final String noHp, final String alamat) {
        auth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(RegisActivity.this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()){
                            FirebaseUser firebaseUser = auth.getCurrentUser();
                            String userID = firebaseUser.getUid();

                            reference = FirebaseDatabase.getInstance().getReference().child("Users").child(userID);
                            HashMap<String, Object> map = new HashMap<>();
                            map.put("id", userID);
                            map.put("Username", username.toLowerCase());
                            map.put("Nama", nama);
                            map.put("Email", email);
                            map.put("NoHp", noHp);
                            map.put("Alamat", alamat);
                            map.put("imageurl", "https://firebasestorage.googleapis.com/v0/b/farming-e517d.appspot.com/o/Logo.png?alt=media&token=413738c3-83f7-45a1-a6a9-98afe9224578");

                            reference.setValue(map).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isSuccessful()){
                                        pd.dismiss();
                                        Intent intent = new Intent(RegisActivity.this, MainActivity.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(intent);
                                    }
                                }
                            });
                        }else {
                            pd.dismiss();
                            Toast.makeText(RegisActivity.this, "You can't register with this email or password", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    public void Login(View view) {
        startActivity(new Intent(this, LoginActivity.class));
    }
}
