package com.example.iandzillanmalik.farming_app_test;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;

public class SplashScreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //menghilangkan ActionBar
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.activity_splash_screen);
        new android.os.Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(SplashScreen.this, LoginActivity.class));
                finish();
            }
        },3000); //3000 L = 3 detik
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(SplashScreen.class.getSimpleName(), "Onstart");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(SplashScreen.class.getSimpleName(), "OnPause");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(SplashScreen.class.getSimpleName(), "OnResume");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(SplashScreen.class.getSimpleName(), "OnStop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(SplashScreen.class.getSimpleName(), "OnDestroy");
    }
}
