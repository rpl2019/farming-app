package com.example.iandzillanmalik.farming_app_test.Model;

public class User {
    private String id;
    private String Username;
    private String Nama;
    private String imageurl;
    private String NoHp;
    private String Alamat;
    private String Email;

    public User(String id, String Username, String Nama, String imageurl, String NoHp, String Alamat, String Email) {
        this.id = id;
        this.Username = Username;
        this.Nama = Nama;
        this.imageurl = imageurl;
        this.NoHp = NoHp;
        this.Alamat = Alamat;
        this.Email = Email;
    }

    public User() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return Username;
    }

    public void setUsername(String Username) {
        this.Username = Username;
    }

    public String getNama() {
        return Nama;
    }

    public void setNama(String Nama) {
        this.Nama = Nama;
    }

    public String getImageurl() {
        return imageurl;
    }

    public void setImageurl(String imageurl) {
        this.imageurl = imageurl;
    }

    public String getNoHp() {
        return NoHp;
    }

    public void setNoHp(String NoHp) {
        this.NoHp = NoHp;
    }

    public String getAlamat() {
        return Alamat;
    }

    public void setAlamat(String Alamat) {
        this.Alamat = Alamat;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String Email) {
        this.Email = Email;
    }
}
