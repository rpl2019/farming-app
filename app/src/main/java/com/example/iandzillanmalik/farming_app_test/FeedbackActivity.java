package com.example.iandzillanmalik.farming_app_test;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;

public class FeedbackActivity extends AppCompatActivity {
    EditText feedback;
    Button send;
    FirebaseUser firebaseUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);

        feedback = findViewById(R.id.editText_feedback);
        send = findViewById(R.id.button_feedback);

        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();

        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (feedback.getText().toString().equals("")){
                    Toast.makeText(FeedbackActivity.this, "You can't send empty feedback", Toast.LENGTH_SHORT).show();
                } else {
                    addFeedback();
                    Toast.makeText(FeedbackActivity.this, "Thanks for your feedback", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(FeedbackActivity.this, MainActivity.class));
                }
            }
        });
    }

    public void addFeedback(){
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Feedbacks").child(firebaseUser.getUid());

        String feedbackid = reference.push().getKey();

        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("feedback", feedback.getText().toString());
        hashMap.put("feedbackid", feedbackid);

        reference.child(feedbackid).setValue(hashMap);
    }
}
